/*1. Функції потрібні,щоб робити однакові дії без повторення коду. */
/*2. У функції є параметри, які ми записуємо у () , ці значення ми передаємо до функції ,коли викликаємо її і у подальшому ці значення використовуються при виконанні цієї функції.
 Аргументи ,які ми передаємо до функції можуть бути ,які завгодно, а може таке бути,що функція працює зовсім без них.*/
/*3. Можна використовувати return без значення. Це призведе до негайного виходу з функції. 
 Директива return може бути в будь-якому місці функції. Коли виконання досягає цієї директиви, функція зупиняється, і в код, який викликав цю функцію, повертається значення, яке прописано зразу після return.*/

let numberFirst = prompt('Введіть будь ласка число');
let numberSecond = prompt('Введіть будь ласка число');
let operator = prompt('Введіть будь ласка математичну операцію, яку потрібно виконати');

function getNumber(numberEntered) {
     let numberFromEntered = Number(numberEntered);
     while (Number.isNaN(numberFromEntered)) {
          numberEntered = prompt('Введіть будь ласка число', numberEntered);
          numberFromEntered = Number(numberEntered);
     }
     return numberFromEntered;
}

function getOperator(operator) {
     let operatorEntered = operator;
     while (operatorEntered != '+' && operatorEntered != '-' && operatorEntered != '*' && operatorEntered != '/') {
          operatorEntered = prompt('Введіть будь ласка математичну операцію, яку потрібно виконати', operatorEntered);
     }
     return operatorEntered;
}

function calculate(numberFirst, numberSecond, operator) {
     let numfirst = getNumber(numberFirst);
     let numSecond = getNumber(numberSecond);
     let oper = getOperator(operator);

     switch (operator) {
          case '+':
               console.log(+numberFirst + (+numberSecond));
               break;
          case '-':
               console.log(numberFirst - numberSecond);
               break;
          case '*':
               console.log(numberFirst * numberSecond);
               break;
          case '/':
               console.log(numberFirst / numberSecond);
               break;
     }
}

calculate(numberFirst, numberSecond, operator);